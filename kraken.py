
import krakenex

import decimal
import time
from datetime import datetime

pair = 'XETHZEUR'
# NOTE: for the (default) 1-minute granularity, the API seems to provide
# data up to 12 hours old only!
since = str(1499000000) # UTC 2017-07-02 12:53:20

k = krakenex.API()
k.load_key('kraken.key')

def now():
    return decimal.Decimal(time.time())

def lineprint(msg, targetlen = 72):
    line = '-'*5 + ' '
    line += str(msg)

    l = len(line)
    if l < targetlen:
        trail = ' ' + '-'*(targetlen-l-1)
        line += trail

    print(line)
    return

def ohlc_prettyprint(ohlc):
    ret = ''
    ret += datetime.utcfromtimestamp(ohlc[0]).strftime('%Y-%m-%d %H:%M:%S')
    ret += '\nopen ' + str(ohlc[1])
    ret += '\nhigh ' + str(ohlc[2])
    ret += '\nlow '  + str(ohlc[3])
    ret += '\nclose '+ str(ohlc[4])
    ret += '\nvwap ' + str(ohlc[5])
    return ret

def log_ohlc():
    lineprint(now())

    before = now()
    try:
        ret = k.query_public('OHLC', data = {'pair': pair, 'since': since})
    except:
        print("Query failed")
        return
    after = now()

    # comment out to track the same "since"
    #since = ret['result']['last']

    # TODO: don't repeat-print if list too short
    bars = ret['result'][pair]
    for b in bars[:5]: print(b)
    print('...')
    for b in bars[-5:]: print(b)
    
    lineprint(after - before)

def get_last_ohlc():
    # lineprint(now())

    before = now()
    try:
        ret = k.query_public('OHLC', data = {'pair': pair, 'since': since})
    except:
        print("Query failed")
        return
    after = now()

    # comment out to track the same "since"
    #since = ret['result']['last']

    # TODO: don't repeat-print if list too short
    bars = ret['result'][pair]
    return ohlc_prettyprint(bars[-1:][0])
    # for b in bars[:5]: print(b)
    # print('...')
    # for b in bars[-5:]: print(b)

    # lineprint(after - before)

def get_ticker_info():
    try:
        ret = k.query_public('Ticker', data = {'pair': pair})
    except:
        print("Query failed")
        return
    return str(ret)