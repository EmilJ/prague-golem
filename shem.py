
import discord
from secrets import discord_token, trade_whitelist
from discord.ext import commands
from kraken import *
import os

bot = commands.Bot(command_prefix='!')

def is_trader(ctx):
    return (ctx.author.id in trade_whitelist)

@bot.command()
async def ping(ctx):
    await ctx.send('pong')

@bot.command()
async def last(ctx):
    ohlc = get_last_ohlc()
    await ctx.send(ohlc)

@bot.command()
async def ticker(ctx):
    ticker = get_ticker_info()
    await ctx.send(ticker)

@bot.command()
async def scream(ctx):
    if (is_trader(ctx)):
        printstring = "I don't want to scream at you, and you are allowed to trade"
    else:
        printstring = "I don't want to scream at you, but you are NOT allowed to trade"
    await ctx.send(printstring)

@bot.command()
async def restart(ctx):
    await ctx.send("Okay, restarting")
    os.exec*()

bot.run(discord_token)